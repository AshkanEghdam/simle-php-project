<?php include("includes/header.php"); ?>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Start Bootstrap</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li>
                        <a href="admin">Admin Area</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1>

                <?php
                  $showAllPublishedPosts = showAllPosts(true);
                  while ($collect = mysqli_fetch_assoc($showAllPublishedPosts)) {
                    ?>
                    <!-- First Blog Post -->
                    <h2>
                        <a href="#"><?php echo $collect['post_title']; ?></a>
                    </h2>
                    <p class="lead">
                        by <a href="index.php"><?php echo $collect['post_author']; ?></a>
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> <?php echo $collect['post_date']; ?></p>
                    <hr>
                    <img class="img-responsive" src="images/<?php echo $collect['post_image']; ?>" alt="">
                    <hr>
                    <p><?php echo mb_substr($collect['post_content'] , 0 , 170); ?></p>
                    <a class="btn btn-primary" href="post.php?id=<?php echo $collect['id']; ?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                    <hr>
                    <?php
                  }
                ?>
                <!-- Pager -->
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <?php include("includes/sidebar.php"); ?>

        </div>
        <!-- /.row -->

        <hr>
<?php include("includes/footer.php"); ?>
