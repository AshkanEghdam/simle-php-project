<?php include("includes/header.php"); ?>
<?php
  if (isset($_GET['id'])) {
    $postId = $_GET['id'];
  }
  if (isset($_POST['sendComments'])) {
    $getPostId = $_GET['id'];
    insertComment($getPostId, $_POST['author'], $_POST['authorEmail'], $_POST['comment']);
  }
?>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Start Bootstrap</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">
              <?php
                global $postId;
                $findPostById = findPostById($postId);
                while ($collect = mysqli_fetch_assoc($findPostById)) {
                  $postTitle = $collect['post_title'];
                  $postAuthor = $collect['post_author'];
                  $postDate = $collect['post_date'];
                  $postImage = $collect['post_image'];
                  $postContent = $collect['post_content'];

                }
              ?>
                <!-- Blog Post -->

                <!-- Title -->
                <h1><?php echo $postTitle; ?></h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="#"><?php echo $postAuthor; ?></a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $postDate; ?> at 9:00 PM</p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" src="images/<?php echo $postImage; ?>" alt="">

                <hr>

                <!-- Post Content -->
                <p class="lead"><?php echo $postContent; ?></p>


                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="post" action="">
                        <div class="form-group">
                          <label for="author">Your name:</label>
                            <input class="form-control" type="text" name="author" value="">
                        </div>
                        <div class="form-group">
                          <label for="authorEmail">Email address:</label>
                            <input class="form-control" type="email" name="authorEmail" value="">
                        </div>
                        <div class="form-group">
                          <label for="comment">Comment:</label>
                            <input class="form-control" type="text" name="comment" value="">
                        </div>
                        <button type="submit" class="btn btn-info" name="sendComments">Send</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <!-- Comment -->


                
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <!-- Blog Categories Well -->
                <?php include("includes/sidebar.php"); ?>

        </div>
        <!-- /.row -->

        <hr>
<?php include("includes/footer.php"); ?>
