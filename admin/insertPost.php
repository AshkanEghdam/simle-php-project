<?php include("includes/header.php"); ?>
<?php
  if (isset($_POST['AddPost'])) {

      $postTitle = $_POST['postTitle'];
      $postTags = $_POST['postTags'];
      $postCategory = $_POST['postCategory'];
      $postAuthor = $_POST['postAuthor'];
      $postStatus = $_POST['postStatus'];
      $postContent = $_POST['postContent'];
      $imagePostName = $_FILES['userFile']['name'];
      $imagePostFileTmp = $_FILES['userFile']['tmp_name'];
      move_uploaded_file($imagePostFileTmp , "../images/$imagePostName");
      $postDate = date('y-m-d');

      $insertQuery = "INSERT INTO posts (post_category_id, post_title, post_author, post_date, post_image, post_content, post_tags, status) ";
      $insertQuery .= "VALUES('{$postCategory}','{$postTitle}' , '{$postAuthor}', now(), '{$imagePostName}', '{$postContent}', '{$postTags}', '{$postStatus}')";
      $result = mysqli_query($connectionDB , $insertQuery);
      if (!$result) {
        die("Your connection have problem! " . mysqli_error($connectionDB));
      }


  }
?>

<div id="wrapper">

  <!-- Navigation -->
  <?php include("includes/navigation.php") ?>

  <div id="page-wrapper">

    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
            Welcome to admin area
            <small>Edit Posts</small>
          </h1>

        </div>
      </div>

      <div class="col-md-12">
      <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

        <div class="form-group">
            <label for="exampleInputEmail1">Post Title</label>
            <input name="postTitle" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Title" value="">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Post Tags</label>
            <input name="postTags" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Tags" value="">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Post Category</label>
            <select class='form-control' id="postCategory" name="postCategory">
              <?php
                $selectAllCategories = findCategories();
                while ($rows = mysqli_fetch_assoc($selectAllCategories)) {
                    echo "<option value='{$rows['id']}'>{$rows['Category']}</option>";
                }
              ?>

            </select>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Post author</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="post author" value="" name="postAuthor">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Post status</label>
            <select class='form-control' id="postStatus" name="postStatus">
              <option value="draft">Draft</option>
              <option value="publish">Publish</option>
            </select>
        </div>
        <textarea name="postContent" id="" cols="70" rows="12"></textarea>

        <img src="" alt="">
        <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" id="exampleInputFile" name="userFile">

        </div>

        <button class="btn btn-info" type="submit" class="btn btn-default" name="AddPost">Add Post</button>
        </form>
    </div>

    <!-- /#page-wrapper -->
    <?php include("includes/footer.php"); ?>
