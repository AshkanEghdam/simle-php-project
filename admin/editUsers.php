<?php include("includes/header.php"); ?>
<?php
  if (isset($_GET['editUser'])) {
    $getUser = findUserById($_GET['editUser']);
    while ($collectUser = mysqli_fetch_assoc($getUser)) {
      $username = $collectUser['username'];
      $password = $collectUser['password'];
      $firstName = $collectUser['firstname'];
      $lastName = $collectUser['lastname'];
      $userImage = $collectUser['user_image'];
      $email = $collectUser['email'];
      $role = $collectUser['role'];
    }
  }

  if (isset($_POST['editUser'])) {
    $editUsername = $_POST['username'];
    $editPassword = $_POST['password'];
    $editFirstname = $_POST['firstname'];
    $editLastname = $_POST['lastname'];
    $editUserRole = $_POST['userRole'];
    $editEmail = $_POST['userEmail'];
    $userImageName = $_FILES['userFile']['name'];
    $userImageTMP = $_FILES['userFile']['tmp_name'];
    move_uploaded_file($userImageTMP, 'images/$userImageName');

    
  }
?>

<div id="wrapper">

  <!-- Navigation -->
  <?php include("includes/navigation.php") ?>

  <div id="page-wrapper">

    <div class="container-fluid">
      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
            Welcome to admin area
            <small>Edit User</small>
          </h1>
        </div>
      </div>

      <div class="col-md-12">
      <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input name="username" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Title" value="<?php echo $username; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Password</label>
            <input name="password" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Tags" value="<?php echo $password; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input name="firstname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Tags" value="<?php echo $firstName; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input name="lastname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Tags" value="<?php echo $lastName; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">User Role</label>
            <select class='form-control' id="postCategory" name="userRole">
              <?php
                if ($role == 'admin') {
                  echo "<option value=\"admin\">Admin</option>";
                  echo "<option value=\"subscriber\">Subscriber</option>";
                  echo "<option value=\"user\">Subscriber</option>";
                }elseif ($role == 'subscriber') {
                  echo "<option value=\"subscriber\">Subscriber</option>";
                  echo "<option value=\"admin\">Admin</option>";
                  echo "<option value=\"user\">Subscriber</option>";
                }else {
                  echo "<option value=\"user\">Subscriber</option>";
                  echo "<option value=\"subscriber\">Subscriber</option>";
                  echo "<option value=\"admin\">Admin</option>";
                }
              ?>
            </select>

        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">User Email</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="post author" value="<?php echo $email; ?>" name="userEmail">
        </div>

        <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" id="exampleInputFile" name="userFile">
        </div>

        <button class="btn btn-info" type="submit" class="btn btn-default" name="editUser">Edit User</button>
        </form>
    </div>

    <!-- /#page-wrapper -->
    <?php include("includes/footer.php"); ?>
