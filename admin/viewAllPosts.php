<?php include("includes/header.php"); ?>
<?php
  if(isset($_GET['deletePost'])){
    deletePostById($_GET['deletePost']);
  }

  if (isset($_GET['publish'])) {
    $postPublishId = $_GET['publish'];
    publishPost($postPublishId);
  }

  if (isset($_GET['draft'])) {
    $postDraftId = $_GET['draft'];
    draftPost($postDraftId);
  }
?>


<div id="wrapper">

  <!-- Navigation -->
  <?php include("includes/navigation.php") ?>

  <div id="page-wrapper">

    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
            Welcome to admin area
            <small>Posts</small>
          </h1>

        </div>
      </div>

       <div class="col-md-12">
        <table class="table table-hover ">
          <th>Id</th>
          <th>Post Category</th>
          <th>Post Title</th>
          <th>Post Author</th>
          <th>Image</th>
          <th>Post Date</th>
          <th>Comment Count</th>
          <th>Status</th>
          <th>Publish</th>
          <th>Draft</th>
          <th>Edit</th>
          <th>Delete</th>

          <?php
          $id = 0;
          $readAllPosts = viewAllPosts();
          while ($collect = mysqli_fetch_assoc($readAllPosts)) {
          ?>
            <tr>
              <td><?php echo ++$id; ?></td>
              <?php
              $query = "SELECT * FROM categories WHERE id = {$collect['post_category_id']}";
              $queryConnection = mysqli_query($GLOBALS['connectionDB'], $query);
              while ($callCategory = mysqli_fetch_assoc($queryConnection)) {
                echo "<td>{$callCategory['Category']}</td>";
              }

              ?>

              <td><?php echo $collect['post_title']; ?></td>
              <td><?php echo $collect['post_author']; ?></td>
              <td><img src="../images/<?php echo $collect['post_image']; ?>" alt="Post Image" height="50" width="100"></td>
              <td><?php echo $collect['post_date']; ?></td>
              <td><?php echo $collect['comment_count']; ?></td>
              <td><?php echo $collect['status']; ?></td>
              <td>
                <a href="viewAllPosts.php?publish=<?php echo $collect['id']; ?>">Publish</a>
              </td>
              <td>
                <a href="viewAllPosts.php?draft=<?php echo $collect['id']; ?>">Draft</a>
              </td>
              <td>
                <button class="btn btn-info" type="button" name="editPost" onclick="window.location.href='editPost.php?editPost=<?php echo $collect['id']; ?>'">Edit</button>
              </td>
              <td>
                <button class="btn btn-danger" type="button" name="deletePost" onclick="window.location.href ='viewAllPosts.php?deletePost=<?php echo $collect['id']; ?>'">Delete</button>
              </td>
            </tr>
          <?php
          }
          ?>
        </table>

        <!-- /.container-fluid -->

    </div>

    <!-- /#page-wrapper -->
    <?php include("includes/footer.php"); ?>
