<?php include("includes/header.php"); ?>
<?php
  if (isset($_GET['Approve'])) {
    $approvedId = commentApproveById($_GET['Approve']);
  }elseif (isset($_GET['Unapprove'])) {
    $unapprovedId = commentUnapproveById($_GET['Unapprove']);
  }elseif (isset($_GET['Delete'])) {
    $deleteComment = deleteCommentById($_GET['Delete']);
  }

?>
    <div id="wrapper">

        <!-- Navigation -->
        <?php include("includes/navigation.php") ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome to admin area
                            <small>Comments Area</small>
                        </h1>

                    </div>
                </div>

                <!-- /.row -->
                <div class="col-md-12">
                  <table class="table table-striped">
                    <tr>
                      <th>Id</th>
                      <th>Post id</th>
                      <th>Author</th>
                      <th>Email</th>
                      <th>Content</th>
                      <th>Status</th>
                      <th>Date</th>
                      <th>Approved</th>
                      <th>Unapproved</th>
                      <th>Delete</th>
                    </tr>

                      <?php
                        $readAllComments = collectAllComments();
                        while ($collect = mysqli_fetch_assoc($readAllComments)) {
                            $commectId = $collect['id'];
                            $postId = $collect['comment_post_id'];
                            $commectAuthor = $collect['comment_author'];
                            $commectEmail = $collect['comment_email'];
                            $commectContent = $collect['content'];
                            $commentStatus = $collect['status'];
                            $commentDate = $collect['date'];
                            ?>

                            <tr>
                              <td><?php echo $commectId; ?></td>
                              <td><?php echo $postId; ?></td>
                              <td><?php echo $commectAuthor; ?></td>
                              <td><?php echo $commectEmail; ?></td>
                              <td><?php echo $commectContent; ?></td>
                              <td><?php echo $commentStatus ?></td>
                              <td><?php echo $commentDate; ?></td>
                              <td><button class="btn btn-success" type="button"  onclick="window.location.href='comments.php?Approve=<?php echo $commectId; ?>'">Approved</button></td>
                              <td><button class="btn btn-warning" type="button" onclick="window.location.href='comments.php?Unapprove=<?php echo $commectId; ?>'">Unapproved</button></td>
                              <td><button class="btn btn-danger" type="button" onclick="window.location.href='comments.php?Delete=<?php echo $commectId; ?>'">Delete</button></td>
                            </tr>
                        <?php
                        }
                      ?>


                  </table>
              </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
<?php include("includes/footer.php"); ?>
