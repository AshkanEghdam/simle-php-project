<?php include("includes/header.php"); ?>
<?php
  if (isset($_GET['id'])) {
    deleteCategoriesById($_GET['id']);
    redirect_to("categories.php");
  }
  if (isset($_POST['submit'])) {
    $catTitle = $_POST['cat_title'];
    if (!empty($catTitle)) {
      addCategory($catTitle);
      redirect_to("categories.php");
    }
  }
?>
    <div id="wrapper">

        <!-- Navigation -->
        <?php include("includes/navigation.php") ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome to admin area
                            <small>Categories</small>
                        </h1>

                    </div>
                </div>

                <!-- /.row -->
                <div class="col-xs-6">
                  <table class="table table-hover ">
                    <th>ID</th>
                    <th>Title</th>
                    <th>Delete</th>
                    <?php
                    $id = 0;
                      $allCategories = findCategories();
                      while ($collect = mysqli_fetch_assoc($allCategories)) {
                        $GLOBALS['id'] = ++$id;
                        $regularId = $collect['id'];
                        $title = $collect['Category'];
                        ?>
                          <tr>
                        <td><?php echo $id; ?></td>
                        <td><?php echo $title; ?></td>
                        <td><button type="button" class="btn btn-danger" onclick="window.location.href='categories.php?id=<?php echo $regularId; ?>'">Delete</button></td>
                        </tr>
                      <?php
                      }
                    ?>
                </div>

                  <div class="jumbotron">

                      <form class="" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                        <?php
                          if (isset($_POST['cat_title'])) {
                            $getCat = $_POST['cat_title'];
                            if (empty($getCat)) {
                              echo "<div class='alert alert-danger' role='alert'><em>This filed can't be blank!</em></div>";
                            }
                          }
                          
                        ?>
                        
                        <div class="form-group">
                          <label for="cat_title">Add Category</label>
                          <input class="form-control" id="cat_title" type="text" name="cat_title" value="">
                        </div>
                        <div class="form-group">
                          <input class="btn btn-success" type="submit" name="submit" value="Submit">
                        </div>
                      </form>


                  </div>

                </div>

                </table>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
<?php include("includes/footer.php"); ?>
