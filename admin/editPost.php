<?php include("includes/header.php"); ?>
<?php
  if (isset($_GET['editPost'])) {
    $editPostId = $_GET['editPost'];
    $editPostInfo = findPostById($_GET['editPost']);
    while ($collect = mysqli_fetch_assoc($editPostInfo)) {
      $postCategory = $collect['post_category_id'];
      $postTitle = $collect['post_title'];
      $postAuthor = $collect['post_author'];
      $postDate = $collect['post_date'];
      $postImage = $collect['post_image'];
      $postContent = $collect['post_content'];
      $postTags = $collect['post_tags'];
      $postStatus = $collect['status'];
    }
  }

  if (isset($_POST['edit'])) {
    $editPostTitle = $_POST['postTitle'];
    $editPostCategories = $_POST['postCategory'];
    $editPostAuthor = $_POST['postAuthor'];
    $editPostContent = $_POST['postContent'];
    $editPostTags = $_POST['postTags'];
    $editPostImageName = $_FILES['postImage']['name'];
    $editPostImageTMP = $_FILES['postImage']['tmp_name'];
    move_uploaded_file($editPostImageTMP , '../images/$editPostImageName');


    if (empty($editPostImageName)) {
      $findImageQuery = "SELECT * FROM posts WHERE id = $editPostId";
      $result = mysqli_query($connectionDB, $findImageQuery);
      if (!$result) {
        die("Your query have problem! " . mysqli_error($connectionDB));
      }
      while ($imageCollection = mysqli_fetch_assoc($result)) {
        $editPostImageName = $imageCollection['post_image'];
      }
    }

    $updateQuery = "UPDATE posts SET ";
    $updateQuery .= "post_category_id = {$editPostCategories}, ";
    $updateQuery .= "post_title = '{$editPostTitle}', ";
    $updateQuery .= "post_Author = '{$editPostAuthor}', ";
    $updateQuery .= "post_image = '{$editPostImageName}', ";
    $updateQuery .= "post_tags = '{$editPostTags}', ";
    $updateQuery .= "post_content = '{$editPostContent}'";
    $updateQuery .= "WHERE id = {$_GET['editPost']}";

    $queryConnection = mysqli_query($connectionDB , $updateQuery);
    if (!$queryConnection && mysqli_affected_rows($connectionDB)) {
      die("Your update query have problem! " . mysqli_error($connectionDB));
    }
    redirect_to("viewAllPosts.php");
  }

?>


<div id="wrapper">

  <!-- Navigation -->
  <?php include("includes/navigation.php") ?>

  <div id="page-wrapper">

    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
            Welcome to admin area
            <small>Edit Post</small>
          </h1>

        </div>
      </div>

      <div class="col-md-12">
      <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

        <div class="form-group">
            <label for="exampleInputEmail1">Post Title</label>
            <input name="postTitle" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Title" value="<?php echo $postTitle; ?>">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Post Category</label>
            <select class='form-control' id="postCategory" name="postCategory">
              <?php
                $findCategory = findCategoriesById($postCategory);
                while ($collect = mysqli_fetch_assoc($findCategory)) {
                  $postCategoriesTitle = $collect['Category'];
                  $postCategoiresId = $collect['id'];
                  echo "<option value=\"$postCategoiresId \">$postCategoiresId . $postCategoriesTitle</option>";
                }

                $findAllCategories = findCategories();
                while ($getAllCategories = mysqli_fetch_assoc($findAllCategories)) {
                  $categoriesTitle = $getAllCategories['Category'];
                  $categoriesId = $getAllCategories['id'];
                  if ($categoriesTitle == $postCategoriesTitle) {
                    continue;
                  }else {
                      echo "<option value=\"$categoriesId\">$categoriesId . $categoriesTitle</option>";
                  }

                  }

              ?>

            </select>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Post author</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="post author" value="<?php echo $postAuthor; ?>" name="postAuthor">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Post Tags</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="post Tags" value="<?php echo $postTags; ?>" name="postTags">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Post status</label>
            <select class='form-control' id="postStatus" name="postStatus">
              <?php
                if ($postStatus == 'draft') {
                  echo "<option value=\"draft\">Draft</option>";
                  echo "<option value=\"publish\">Publish</option>";
                }else {
                  echo "<option value=\"publish\">Publish</option>";
                  echo "<option value=\"draft\">Draft</option>";
                }
              ?>

            </select>
        </div>

        <div class="form-group">
          <textarea name="postContent" id="" cols="50" rows="10"><?php echo $postContent; ?></textarea>
          <img src="../images/<?php echo $postImage; ?>" alt="" width="300" height="150">
        </div>
        <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" id="exampleInputFile" name="postImage">

        </div>

        <button type="submit" class="btn btn-primary" name="edit">Edit Post</button>
        </form>
    </div>

    <!-- /#page-wrapper -->
    <?php include("includes/footer.php"); ?>
