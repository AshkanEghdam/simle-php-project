<?php include("includes/header.php"); ?>
<?php
  if (isset($_GET['delete'])) {
    deleteUserById($_GET['delete']);
    redirect_to("users.php");
  }
?>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include("includes/navigation.php") ?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome to admin area
                            <small>View All Users</small>
                        </h1>

                    </div>
                </div>

                <!-- /.row -->
                <div class="col-md-12">
                  <table class="table table-hover ">
                    <th>ID</th>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>User Image</th>
                    <th>Email</th>
                    <th>User Role</th>
                    <th>Edite</th>
                    <th>DELETE</th>

                    <?php
                    $id = 0;
                      $getUsers = findAllUsers();
                      while ($collect = mysqli_fetch_assoc($getUsers)) {
                        ?>
                        <tr>
                          <td><?php echo ++$id; ?></td>
                          <td><?php echo $collect['username']; ?></td>
                          <td><?php echo $collect['firstname']; ?></td>
                          <td><?php echo $collect['lastname']; ?></td>
                          <td><img alt="" src="images/<?php echo $collect['user_image']; ?>" width="50" height="50"></td>
                          <td><?php echo $collect['email']; ?></td>
                          <td><?php echo $collect['role']; ?></td>
                          <td>

                            <button class="btn btn-info" type="button" name="button" onclick="window.location.href = 'editUsers.php?editUser=<?php echo $collect['id']; ?>'">Edit</button>
                          </td>
                          <td>
                            <button class="btn btn-danger" type="button" name="button" onclick="window.location.href = 'users.php?delete=<?php echo $collect['id']; ?>'">Delete</button>
                          </td>
                        </tr>
                        <?php
                      }
                    ?>
                </div>
                </div>

                </table>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
<?php include("includes/footer.php"); ?>
