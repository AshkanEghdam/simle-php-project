<?php include("includes/header.php"); ?>


<div id="wrapper">

  <!-- Navigation -->
  <?php include("includes/navigation.php") ?>

  <div id="page-wrapper">

    <div class="container-fluid">

      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
            Welcome to admin area
            <small>Add User</small>
          </h1>

        </div>
      </div>
      <?php
        if (isset($_POST['AddUser'])) {
          $username = $_POST['username'];
          $greatUsername = mysqli_prep($username);
          $password = $_POST['password'];
          $greatPassword = mysqli_prep($password);
          $hashFormat = "$2y$10$";
          $salt = "Salt22CharactersOrMore";
          $format_and_salt = $hashFormat . $salt;
          $hashPassword = crypt($greatPassword , $format_and_salt);
          $firstName = $_POST['firstname'];
          $lastname = $_POST['lastname'];
          $userRole = $_POST['userRole'];
          $userEmail = $_POST['userEmail'];
          $userImage = $_FILES['userFile']['name'];
          $userImageTMP = $_FILES['userFile']['tmp_name'];
          move_uploaded_file($userImageTMP , "images/$userImage");

          if (checkUsername($username)) {
            insertUser($greatUsername, $hashPassword, $firstName, $lastname, $userImage, $userRole,$userEmail);
            redirect_to("users.php");
          }else {
            echo "<div class=\"alert alert-danger\" role=\"alert\"> This Username Already exist!</div>";
          }
        }

      ?>

      <div class="col-md-12">
      <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">

        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input name="username" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Title" value="">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Password</label>
            <input name="password" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Tags" value="">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input name="firstname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Tags" value="">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input name="lastname" type="text" class="form-control" id="exampleInputEmail1" placeholder="Post Tags" value="">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">User Role</label>
            <select class='form-control' id="postCategory" name="userRole">
              <option value="admin">Admin</option>
              <option value="user">User</option>
              <option value="subscriber">Subscriber</option>
            </select>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">User Email</label>
            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="post author" value="" name="userEmail">
        </div>

        <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" id="exampleInputFile" name="userFile">
        </div>

        <button class="btn btn-info" type="submit" class="btn btn-default" name="AddUser">Add User</button>
        </form>
    </div>

    <!-- /#page-wrapper -->
    <?php include("includes/footer.php"); ?>
