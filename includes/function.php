<?php

  function redirect_to($newAddress){
    header("Location: " . $newAddress);
    exit;
  }

  function mysqli_prep($variable){
    global $connectionDB;

    $validSqli = mysqli_real_escape_string($connectionDB , $variable);
    return $validSqli;
  }

  function confirmSqliConnection($connection){
    global $connectionDB;

    if (!$connection) {
      die("Your connection have problem " . mysqli_error($connectionDB));
    }
  }

  function findCategories(){
    global $connectionDB;
    $query = "SELECT * FROM categories ORDER BY id ASC";
    $queryConnection = mysqli_query($connectionDB , $query);
    $result = confirmSqliConnection($queryConnection);
    return $queryConnection;
  }

  function findCategoriesById($categoriesId){
    global $connectionDB;
    $query = "SELECT * FROM categories WHERE id = '{$categoriesId}'";
    $queryConnection = mysqli_query($connectionDB , $query);
    $result = confirmSqliConnection($queryConnection);
    return $queryConnection;
  }

  function showAllPosts($visible = true){
    global $connectionDB;
    $postQuery = "SELECT * FROM posts";
    if ($visible) {
      $postQuery .= " WHERE STATUS = 'publish'";
    }
    $postQuery .= " ORDER BY id ASC";
    $postQueryConnection = mysqli_query($connectionDB , $postQuery);
    $result = confirmSqliConnection($postQueryConnection);
    return $postQueryConnection;
  }

  function findPostById($id){
    global $connectionDB;

    $query = "SELECT * FROM posts";
    $query .= " WHERE id = {$id}";
    $queryConnection = mysqli_query($connectionDB , $query);
    $result = confirmSqliConnection($queryConnection);
    return $queryConnection;
  }

  function deleteCategoriesById($id) {
    $query = "DELETE FROM categories WHERE id = {$id}";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $query);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function addCategory($categoryName) {
    $query = "INSERT INTO categories ";
    $query .= "(Category) VALUES ";
    $query .= "('{$categoryName}')";

    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $query);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function findAllUsers(){
    $query = "SELECT * FROM users";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $query);
    $result = confirmSqliConnection($queryConnection);
    return $queryConnection;
  }

  function deleteUserById($userId){
    $query = "DELETE FROM users ";
    $query .= " WHERE id = {$userId}";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $query);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function viewAllPosts(){
    $postQuery = "SELECT * FROM posts";
    $postQuery .=" ORDER BY status DESC";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $postQuery);
    $result = confirmSqliConnection($queryConnection);
    return $queryConnection;
  }

  function deletePostByid($id){
    $deletePostQuery = "DELETE FROM posts WHERE id = {$id}";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $deletePostQuery);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function publishPost($id){
    $publishQuery = "UPDATE posts SET status = 'publish' WHERE id = {$id}";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'], $publishQuery);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function draftPost($id){
    $draftQuery = "UPDATE posts SET status = 'draft' WHERE id = {$id}";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $draftQuery);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function checkUsername($username){
    $checkUsername = "SELECT username FROM users";
    $result = mysqli_query($GLOBALS['connectionDB'] , $checkUsername);
    if (!$result) {
      die("UserName check query have problem!" . mysqli_error($GLOBALS['connectionDB']));
    }

    while ($collectUsername = mysqli_fetch_assoc($result)) {
      $usernameChecked = $collectUsername['username'];
      if ($usernameChecked === $username) {
        return false;
        break;
      }
    }
    return true;
  }

  function insertUser($username, $password, $firstName, $lastname, $userImage, $userRole, $userEmail){
    $insertUser = "INSERT INTO users ";
    $insertUser .= "(username, password, firstname, lastname, user_image, email, role) ";
    $insertUser .= "VALUES ('{$username}', '{$password}', '{$firstName}', '{$lastname}', '{$userImage}', '{$userEmail}', '{$userRole}')";
    $result = mysqli_query($GLOBALS['connectionDB'], $insertUser);
    $result = confirmSqliConnection($result);
    return $result;
  }

  function findUserById($userId){
    $findUserQuery = "SELECT * FROM users WHERE id = {$userId}";
    $findUserConnection = mysqli_query($GLOBALS['connectionDB'] , $findUserQuery);
    $result = confirmSqliConnection($findUserConnection);
    return $findUserConnection;
  }

  function findUserByUsername($username){
    $findUserQuery = "SELECT * FROM users WHERE username = '{$username}'";
    $result = mysqli_query($GLOBALS['connectionDB'] , $findUserQuery);
    confirmSqliConnection($result);
    return $result;
  }

  function collectAllComments() {
    $readAllCommentsQuery = "SELECT * FROM comments";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $readAllCommentsQuery);
    $result = confirmSqliConnection($queryConnection);
    return $queryConnection;
  }

  function commentApproveById($id) {
    $updateQuery = "UPDATE comments SET status = 'approved' WHERE id = {$id}";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $updateQuery);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function commentUnapproveById($id) {
    $updateQuery = "UPDATE comments SET status = 'unapproved' WHERE id = {$id}";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $updateQuery);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function deleteCommentById($id) {
    $deleteQuery = "DELETE FROM comments WHERE id = {$id}";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $deleteQuery);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function insertComment($postId, $authorName, $authorEmail, $commentContent) {
    $insertCommentQuery = "INSERT INTO comments (comment_post_id , comment_author, comment_email, content, status, date)";
    $insertCommentQuery .= "VALUES ({$postId}, '{$authorName}', '{$authorEmail}', '{$commentContent}', 'unapproved' , now())";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'] , $insertCommentQuery);
    $result = confirmSqliConnection($queryConnection);
    return $result;
  }

  function callAllPosts(){
    $callPosts = "SELECT * FROM posts";
    $result = mysqli_query($GLOBALS['connectionDB'], $callPosts);
    confirmSqliConnection($result);
    $allPosts = mysqli_num_rows($result);
    return $allPosts;
  }

  function callAllComments(){
    $callAllCommectsQuery = "SELECT * FROM comments";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'], $callAllCommectsQuery);
    $result = confirmSqliConnection($queryConnection);
    $callAllComments = mysqli_num_rows($queryConnection);
    return $callAllComments;
  }

  function callAllUsers(){
    $callAllUsersQuery = "SELECT * FROM users";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'], $callAllUsersQuery);
    $result = confirmSqliConnection($queryConnection);
    $allUsers = mysqli_num_rows($queryConnection);
    return $allUsers;
  }

  function callAllCategories(){
    $callAllCategoriesQuery = "SELECT * FROM categories";
    $queryConnection = mysqli_query($GLOBALS['connectionDB'], $callAllCategoriesQuery);
    $result = confirmSqliConnection($queryConnection);
    $allCategories = mysqli_num_rows($queryConnection);
    return $allCategories;
  }


?>
