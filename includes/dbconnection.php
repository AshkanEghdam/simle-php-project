<?php

  $serveName = "localhost";
  $serverUsername = "root";
  $serverPassword = "";
  $serverDatabaseName = "cms";

  $connectionDB = mysqli_connect($serveName , $serverUsername, $serverPassword, $serverDatabaseName);

  if (mysqli_connect_error()) {
    die("Your connection Database have problem! " . mysqli_connection_error($connectionDB));
  }
?>
