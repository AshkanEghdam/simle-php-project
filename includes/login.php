<?php session_start(); ?>
<?php include "dbconnection.php"; ?>
<?php include "function.php"; ?>
<?php
  if (isset($_POST['loginBTN'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $strongUsername = mysqli_prep($username);
    $strongPassword = mysqli_prep($password);

    $collectUserFromDatabase = findUserByUsername($strongUsername);
    while ($collectUser = mysqli_fetch_assoc($collectUserFromDatabase)) {
      $userPassword = $collectUser['password'];
      $userUsername = $collectUser['username'];
      $userRole = $collectUser['role'];
    }


    if ($userPassword === $strongPassword) {

      $_SESSION['username'] = $userUsername;
      $_SESSION['role'] = $userRole;
      redirect_to("../admin");
    }else {
      redirect_to("../index.php");
    }
  }
?>
