<div class="well">
    <h4>Login</h4>
    <form class="" action="includes/login.php" method="post">
      <div class="form-group">
        <input class="form-control" type="text" name="username" value="username">
      </div>

      <div class="input-group">
        <input class="form-control" type="password" name="password" value="password">
        <samp class="input-group-btn">
          <button class="btn btn-info" type="submit" name="loginBTN">Login</button>
        </samp>
      </div>
    </form>
    <br>
    <h4>Blog Search</h4>
    <div class="input-group">
        <input type="text" class="form-control">
        <span class="input-group-btn">
            <button class="btn btn-default" type="button">
                <span class="glyphicon glyphicon-search"></span>
        </button>
        </span>
    </div>
    <!-- /.input-group -->
</div>

<!-- Blog Categories Well -->
<div class="well">
    <h4>Blog Categories</h4>
    <div class="row">
        <div class="col-lg-6">
            <ul class="list-unstyled">
              <?php
                $showAllCategories = findCategories();
                while ($collect = mysqli_fetch_assoc($showAllCategories)) {
                  echo "<li><a href=''>{$collect['Category']}</a></li>";
                }
              ?>
            </ul>
        </div>
        <!-- /.col-lg-6 -->

        <!-- /.col-lg-6 -->
    </div>
    <!-- /.row -->
</div>

<!-- Side Widget Well -->
<div class="well">
    <h4>Side Widget Well</h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
</div>

</div>
